(ns plf03.core)

(defn función-comp-1
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (* 3 x))
        z (comp f g)]
    (z 10)))

(defn función-comp-2
  []
  (let [f (fn [x] (str x))
        g (fn [x] (+ 2 x))
        z (comp f g)]
    (z 8)))

(defn función-comp-3
  []
  (let [f (fn [x] (str x))
        g (fn [x] (dec x))
        h (fn [x] (inc x))
        z (comp f g h)]
    (z 8)))

(defn función-comp-4
  []
  (let [f (fn [x] (true? x))
        z (comp f)]
    (z 2)))

(defn función-comp-5
  []
  (let [f (fn [x] (take 2 x))
        g (fn [x] (str x))
        z (comp f g)]
    (z 12345)))

(defn función-comp-6
  []
  (let [f (fn [x] (char? x))
        g (fn [x] (inc x))
        z (comp f g)]
    (z 10)))

(defn función-comp-7
  []
  (let [f (fn [x] (char? x))
        g (fn [x] (str x))
        z (comp f g)]
    (z 1)))

(defn función-comp-8
  []
  (let [f (fn [x] (dec x))
        g (fn [x] (- 3 x))
        z (comp f g)]
    (z 10)))

(defn función-comp-9
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (+ 3 x))
        z (comp f g)]
    (z 10)))

(defn función-comp-10
  []
  (let [f (fn [x] (rational? x))
        g (fn [x] (/ x 3))
        z (comp f g)]
    (z 5)))

(defn función-comp-11
  []
  (let [f (fn [x] (float? x))
        g (fn [x] (+ 5 x))
        z (comp f g)]
    (z 5.0)))

(defn función-comp-12
  []
  (let [f (fn [x] (integer? x))
        g (fn [x] (* 2 x))
        z (comp f g)]
    (z 1)))

(defn función-comp-13
  []
  (let [f (fn [x] (seqable? x))
        g (fn [x] (+ 2 x ))
        z (comp f g)]
    (z 5)))

(defn función-comp-14
  []
  (let [f (fn [x] (sequential? x))
        g (fn [x] (list x))
        z (comp f g)]
    (z 123)))

(defn función-comp-15
  []
  (let [f (fn [x] (sequential? x))
        g (fn [x] (str x))
        z (comp f g)]
    (z 123)))

(defn función-comp-16
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (+ 1 x))
        z (comp f g)]
    (z 123)))

(defn función-comp-17
  []
  (let [f (fn [x] (concat x "prueba"))
        z (comp f)]
    (z "hola")))

(defn función-comp-18
  []
  (let [f (fn [x] (int? x))
        z (comp f)]
    (z 1)))

(defn función-comp-19
  []
  (let [f (fn [x] (repeat 2 x))
        z (comp f)]
    (z 2)))

(defn función-comp-20
  []
  (let [f (fn [x] (reverse x))
        z (comp f)]
    (z "prueba")))

;Ejemplos 
(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)

(defn función-complement-1
  []
  (let [f (fn [x] (empty? x))
        z (complement f)]
    (z [1 2])))

(defn función-complement-2
  []
  (let [f (fn [x] (empty? x))
        z (complement f)]
    (z [])))

(defn función-complement-3
  []
  (let [f (fn [x] (string? x))
        z (complement f)]
    (z "abc")))

(defn función-complement-4
  []
  (let [f (fn [x] (char? x))
        z (complement f)]
    (z "abc")))

(defn función-complement-5
  []
  (let [f (fn [x] (string? x))
        z (complement f)]
    (z 123)))

(defn función-complement-6
  []
  (let [f (fn [x] (char? x))
        z (complement f)]
    (z {:a 1})))

(defn función-complement-7
  []
  (let [f (fn [x] (map? x))
        z (complement f)]
    (z {:a 1})))

(defn función-complement-8
  []
  (let [f (fn [x] (map? x))
        z (complement f)]
    (z 123)))

(defn función-complement-9
  []
  (let [f (fn [x] (true? x))
        z (complement f)]
    (z true)))

(defn función-complement-10
  []
  (let [f (fn [x] (true? x))
        z (complement f)]
    (z false)))

(defn función-complement-11
  []
  (let [f (fn [x] (false? x))
        z (complement f)]
    (z true)))

(defn función-complement-12
  []
  (let [f (fn [x] (false? x))
        z (complement f)]
    (z false)))

(defn función-complement-13
  []
  (let [f (fn [x] (seqable? x))
        z (complement f)]
    (z 5)))

(defn función-complement-14
  []
  (let [f (fn [x] (sequential? x))
        z (complement f)]
    (z [1 2 3 4])))

(defn función-complement-15
  []
  (let [f (fn [x] (sequential? x))
        z (complement f)]
    (z 1)))

(defn función-complement-16
  []
  (let [f (fn [x] (even? x))
        z (complement f)]
    (z 2)))

(defn función-complement-17
  []
  (let [f (fn [x] (even? x))
        z (complement f)]
    (z 1)))

(defn función-complement-18
  []
  (let [f (fn [x] (int? x))
        z (complement f)]
    (z 1.0)))

(defn función-complement-19
  []
  (let [f (fn [x] (int? x))
        z (complement f)]
    (z 1)))

(defn función-complement-20
  []
  (let [f (fn [x] (float? x))
        z (complement f)]
    (z 1.0)))

;Ejemplos 
(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)

(defn función-constantly-1
  []
  (let [f (fn [x] (empty? x))
        z ((constantly f))]
    (z [1 2 3])))

(defn función-constantly-2
  []
  (let [f (fn [x] (empty? x))
        z ((constantly f))]
    (z nil)))

(defn función-constantly-3
  []
  (let [f (fn [x] (+ 2 x))
        z ((constantly f))]
    (z 1)))

(defn función-constantly-4
  []
  (let [f (fn [x] (- 2 x))
        z ((constantly f))]
    (z 1)))

(defn función-constantly-5
  []
  (let [f (fn [x] (* 2 x))
        z ((constantly f))]
    (z 1)))

(defn función-constantly-6
  []
  (let [f (fn [x] (/ 2 x))
        z ((constantly f))]
    (z 2)))

(defn función-constantly-7
  []
  (let [f (fn [x] (map? x))
        z ((constantly f))]
    (z {:a 1})))

(defn función-constantly-8
  []
  (let [f (fn [x] (map? x))
        z ((constantly f))]
    (z 123)))

(defn función-constantly-9
  []
  (let [f (fn [x] (true? x))
        z ((constantly f))]
    (z true)))

(defn función-constantly-10
  []
  (let [f (fn [x] (true? x))
        z ((constantly f))]
    (z false)))

(defn función-constantly-11
  []
  (let [f (fn [x] (false? x))
        z ((constantly f))]
    (z true)))

(defn función-constantly-12
  []
  (let [f (fn [x] (false? x))
        z ((constantly f))]
    (z false)))

(defn función-constantly-13
  []
  (let [f (fn [x] (seqable? x))
        z ((constantly f))]
    (z 5)))

(defn función-constantly-14
  []
  (let [f (fn [x] (sequential? x))
        z ((constantly f))]
    (z [1 2 3 4])))

(defn función-constantly-15
  []
  (let [f (fn [x] (sequential? x))
        z ((constantly f))]
    (z [4 2 3])))

(defn función-constantly-16
  []
  (let [f (fn [x] (even? x))
        z ((constantly f))]
    (z 27)))

(defn función-constantly-17
  []
  (let [f (fn [x] (even? x))
        z ((constantly f))]
    (z 6)))

(defn función-constantly-18
  []
  (let [f (fn [x] (int? x))
        z ((constantly f))]
    (z 1.0)))

(defn función-constantly-19
  []
  (let [f (fn [x] (int? x))
        z ((constantly f))]
    (z 4)))

(defn función-constantly-20
  []
  (let [f (fn [x] (+ 3 x))
        z ((constantly f))]
    (z 3)))

;Ejemplos 
(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)

(defn función-every-pred-1
  []
  (let [f (fn [x] (empty? x))
        g (fn [x] (list? x))
        z (every-pred f g)]
    (z '())))

(defn función-every-pred-2
  []
  (let [f (fn [x] (empty? x))
        g (fn [f] (list? f))
        z (every-pred f g)]
    (z [1 2 3])))

(defn función-every-pred-3
  []
  (let [f (fn [x] (string? x))
        z (every-pred f)]
    (z "abc")))

(defn función-every-pred-4
  []
  (let [f (fn [x] (char? x))
        z (every-pred f)]
    (z "abc")))

(defn función-every-pred-5
  []
  (let [f (fn [x] (string? x))
        z (every-pred f)]
    (z 123)))

(defn función-every-pred-6
  []
  (let [f (fn [x] (char? x))
        z (every-pred f)]
    (z {:a 1})))

(defn función-every-pred-7
  []
  (let [f (fn [x] (map? x))
        z (every-pred f)]
    (z {:a 1})))

(defn función-every-pred-8
  []
  (let [f (fn [x] (map? x))
        z (every-pred f)]
    (z 123)))

(defn función-every-pred-9
  []
  (let [f (fn [x] (true? x))
        z (every-pred f)]
    (z true)))

(defn función-every-pred-10
  []
  (let [f (fn [x] (true? x))
        z (every-pred f)]
    (z false)))

(defn función-every-pred-11
  []
  (let [f (fn [x] (false? x))
        g (fn [x] (odd? x))
        z (every-pred f g)]
    (z true)))

(defn función-every-pred-12
  []
  (let [f (fn [x] (false? x))
        g (fn [x] (true? x))
        z (every-pred f g)]
    (z false)))

(defn función-every-pred-13
  []
  (let [f (fn [x] (seqable? x))
        g (fn [x] (list? x))
        h (fn [x] (vector? x))
        z (every-pred f g h)]
    (z 5)))

(defn función-every-pred-14
  []
  (let [f (fn [x] (sequential? x))
        g (fn [x] (map? x))
        h (fn [x] (integer? x))
        z (every-pred f g h)]
    (z [1 2 3 4])))

(defn función-every-pred-15
  []
  (let [f (fn [x] (sequential? x))
        g (fn [x] (number? x))
        h (fn [x] (false? x))
        z (every-pred f g h)]
    (z 1)))

(defn función-every-pred-16
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (number? x))
        h (fn [x] (true? x))
        z (every-pred f g h)]
    (z 2)))

(defn función-every-pred-17
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (number? x))
        h (fn [x] (char? x))
        i (fn [x] (string? x))
        z (every-pred f g h i)]
    (z 1)))

(defn función-every-pred-18
  []
  (let [f (fn [x] (int? x))
        g (fn [x] (number? x))
        h (fn [x] (char? x))
        z (every-pred f g h)]
    (z 1.0)))

(defn función-every-pred-19
  []
  (let [f (fn [x] (int? x))
        g (fn [x] (number? x))
        h (fn [x] (float? x))
        i (fn [x] (integer? x))
        z (every-pred f g h i)]
    (z 1)))

(defn función-every-pred-20
  []
  (let [f (fn [x] (float? x))
        g (fn [x] (number? x))
        h (fn [x] (int? x))
        i (fn [x] (list? x))
        j (fn [x] (char? x))
        z (every-pred f g h i j)]
    (z 1.0)))

;Ejemplos 
(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)

(defn función-fnil-1
  []
  (let [f (fn [x] (str "hola " x))
        z (fnil f [1 2])]
    (z "mundo")))

(defn función-fnil-2
  []
  (let [f (fn [x] (replace [:a :b :c :d :e :f] [x]))
        z (fnil f 5)]
    (z nil)))

(defn función-fnil-3
  []
  (let [f (fn [x] (number? x))
        g (fnil f (take-nth 10 (range 10 50)))
        z (fnil f 21)]
    [(z 50) (g nil)]))

(defn función-fnil-4
  []
  (let [f (fn [x] (nil? x))
        z (fnil f (nil? nil))]
    (z nil)))

(defn función-fnil-5
  []
  (let [f (fn [x] (take-last 2 x))
        z (fnil f (take 4 (range 10)))]
    (z [\a \b \c])))

(defn función-fnil-6
  []
  (let [f (fn [x y] (+ x y))
        z (fnil f 7 9)]
    (z 3 5)))

(defn función-fnil-7
  []
  (let [f (fn [x] (map int? x))
        z (fnil f '(1 1/2 1/4))]
    (z #{1 2})))

(defn función-fnil-8
  []
  (let [f (fn [x] (str "abc" x))
        z (fnil f 456)]
    (z 123)))

(defn función-fnil-9
  []
  (let [f (fn [x] (str x))
        g (fnil f "def")
        z (fnil f "hi")]
    [(z {:a 1}) (g nil)]))

(defn función-fnil-10
  []
  (let [f (fn [x] (true? x))
        z (fnil f false)]
    (z true)))

(defn función-fnil-11
  []
  (let [f (fn [x] (take-last 1 x))
        z (fnil f (take-last 20 (range 30 50)))]
    (z [1 2 3 4 5 6 7 8 9 10])))

(defn función-fnil-12
  []
  (let [f (fn [x] (take 2 x))
        z (fnil f (take 2 (range 10)))]
    (z nil)))

(defn función-fnil-13
  []
  (let [f (fn [x] (empty? x))
        g (fn [x] (first x))
        z (fnil f g)]
    [(z [1 2 3 4 5])]))

(defn función-fnil-14
  []
  (let [f (fn [x] (boolean? x))
        z (fnil f "abc")]
    (z {})))

(defn función-fnil-15
  []
  (let [f (fn [x y] (str "Nombres: " x " y " y))
        z (fnil f "Son" "Novios")]
    (z "Juan" "Laura")))

(defn función-fnil-16
  []
  (let [f (fn [w x y] (replace [\a \b \c \d \e \f] [w x y]))
        z (fnil f 123)]
    (z 1 10 2)))

(defn función-fnil-17
  []
  (let [f (fn [x] (str "La vida es una " x))
        z (fnil f "Pero hay que vivirla")]
    (z "Aventura")))

(defn función-fnil-18
  []
  (let [f (fn [x] (inc x))
        g (fn [f] (integer? f))
        z (fnil f g)]
  (z 10)))

(defn función-fnil-19
  []
  (let [f (fn [x] (map true? x))
        z (fnil f (map true? [false true false true]))]
    (z [true false nil "true" "false"])))

(defn función-fnil-20
  []
  (let [f (fn [x] (reverse x))
        z (fnil f "123456")]
    (z "abcdefg")))

;Ejemplos:
(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)

(defn función-juxt-1
  []
  (let [f (fn [x y] (mapv - x y))
        z (juxt f)]
    (z [1 2 3] [4 5 6])))

(defn función-juxt-2
  []
  (let [f (fn [x y] (conj x y))
        z (juxt f)]
    (z #{1 2} [:a :b :c])))

(defn función-juxt-3
  []
  (let [f (fn [x] (take-last 3 x))
        z (juxt :c f)]
    (z {:a 1 :b 2 :c 3})))

(defn función-juxt-4
  []
  (let [f (fn [x] (string? x))
        g (fn [f] (last f))
        z (juxt f g)]
    (z "Prueba")))

(defn función-juxt-5
  []
  (let [f (fn [x] (vector x))
        z (juxt :a f)]
    (z {:a 1 :b 2 :c 3})))

(defn función-juxt-6
  []
  (let [f (fn [x y] (find x y))
        z (juxt :d f)]
    (z  {:a 1 :b 2 :c 3 :d 4} :a)))

(defn función-juxt-7
  []
  (let [f (fn [x] (str x))
        g (fn [f] (count f))
        z (juxt f g)]
    (z [1 \a 2 \b 3 \c])))

(defn función-juxt-8
  []
  (let [f (fn [x] (take 3 x))
        g (fn [f] (drop 4 f))
        z (juxt :f f g)]
    (z {:a 1 :b 2 :c 3 :d 4 :e 5 :f 6})))

(defn función-juxt-9
  []
  (let [f (fn [x] (map? x))
        z (juxt :d f)]
    (z {:a 1 :b 2 :c 3 :d 4 :e 5 :f 6})))

(defn función-juxt-10
  []
  (let [f (fn [x] (keys x))
        z (juxt :Nombre f)]
    (z {:Nombre "Laura" :Apellido "Hernadez"})))

(defn función-juxt-11
  []
  (let [f (fn [x] (vector x))
        g (fn [f] (first f))
        z (juxt f g)]
    (z {:a 1 :b 2})))

(defn función-juxt-12
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (count x))
        z (juxt f g)]
    (z "abcde")))

(defn función-juxt-13
  []
  (let [f (fn [x] (indexed? x))
        z (juxt f)]
    (z "abcde")))

(defn función-juxt-14
  []
  (let [f (fn [x] (reverse x))
        z (juxt :a f)]
    (z {:a "a" :b "b" :c "c" :d "d"})))

(defn función-juxt-15
  []
  (let [f (fn [x] (shuffle x))
        z (juxt f)]
    (z '(1 2 3 4 5))))

(defn función-juxt-16
  []
  (let [f (fn [x y] (split-at x y))
        z (juxt f)]
    (z 2 [1 2 3 4 5 6])))

(defn función-juxt-17
  []
  (let [f (fn [x] (reverse x))
        g (fn [f] (char? f))
        z (juxt f g)]
    (z "abcd")))

(defn función-juxt-18
  []
  (let [f (fn [x] (inc x))
        g (fn [g] (dec g))
        z (juxt f g)]
    (z 2)))

(defn función-juxt-19
  []
  (let [f (fn [x] (into [] x))
        z (juxt :a :c f)]
    (z {:a 1 :b 2 :c 3 :d 4})))

(defn función-juxt-20
  []
  (let [f (fn [x] (vector x))
        g (fn [x] (string? x))
        z (juxt f g)]
    (z "1234")))

;Ejemplos:
(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)

(defn función-partial-1
  []
  (let [f (fn [x y] (range x y))
        z (partial f 2)]
    (z 10)))

(defn función-partial-2
  []
  (let [f (fn [x y] (partition-all x y))
        z (partial f 4)]
    (z (range 10))))

(defn función-partial-3
  []
  (let [f (fn [x y] (not-any? x y))
        z (partial f odd?)]
    (z '(2 4 6 8))))

(defn función-partial-4
  []
  (let [f (fn [x y] (partition-by x y))
        z (partial f even?)]
    (z [1 1 2 2 3 4 5 5])))

(defn función-partial-5
  []
  (let [f (fn [x y] (remove x y))
        z (partial f neg?)]
    (z [-3 -2 -1 0 1 2 3])))

(defn función-partial-6
  []
  (let [f (fn [w x y] (merge-with w y x))
        z (partial f + {:a 5 :b 5 :c 3 :d 4})]
    (z {:a 1 :b 2})))

(defn función-partial-7
  []
  (let [f (fn [v w x y] (merge-with v w x y))
        z (partial f +)]
    (z {:a 1} {:a 2} {:a 3})))

(defn función-partial-8
  []
  (let [f (fn [x y] (merge x y))
        z (partial f {:b 5 :d 4})]
    (z {:a 1 :b 2 :c 3})))

(defn función-partial-9
  []
  (let [f (fn [u v w x y] (vector u v w x y))
        z (partial f 100)]
    (z 200 300 400 500)))

(defn función-partial-10
  []
  (let [f (fn [x y] (every? x y))
        z (partial f string?)]
    (z #{"a" "b" "c" "d"})))

(defn función-partial-11
  []
  (let [f (fn [u v w x y] (hash-map :a u :b v :c w :d x :e y))
        z (partial f 5)]
    (z 4 3 2 1)))

(defn función-partial-12
  []
  (let [f (fn [x y] (zipmap x y))
        z (partial f [:a :b :c])]
    (z ["a" "b" "c"])))

(defn función-partial-13
  []
  (let [f (fn [w x y] (w x y))
        z (partial f -)]
    (z 15 10)))

(defn función-partial-14
  []
  (let [f (fn [x y] (take-nth x y))
        z (partial f 2)]
    (z (range 10))))

(defn función-partial-15
  []
  (let [f (fn [w x y] (w x y))
        z (partial f +)]
    (z 1 2)))

(defn función-partial-16
  []
  (let [f (fn [x y] (group-by x y))
        z (partial f count)]
    (z ["1" "2" "3" "14" "151"])))

(defn función-partial-17
  []
  (let [f (fn [w x y] (w x y))
        z (partial f /)]
    (z 10 5)))

(defn función-partial-18
  []
  (let [f (fn [x y] (into x y))
        z (partial f [])]
    (z {:a 1 :b 2 :c 3})))

(defn función-partial-19
  []
  (let [f (fn [w x y] (w x y))
        z (partial f *)]
    (z 2 3)))

(defn función-partial-20
  []
  (let [f (fn [x y] (drop-while x y))
        z (partial f neg?)]
    (z [-3 -2 -1 0 1 2 3])))

;Ejemplos:
(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)

(defn función-some-fn-1
  []
  (let [f (fn [x] (empty? x))
        z (some-fn f)]
    (z [1 2 3])))

(defn función-some-fn-2
  []
  (let [f (fn [x] (empty? x))
        z (some-fn f)]
    (z [])))

(defn función-some-fn-3
  []
  (let [f (fn [x] (map? x))
        z (some-fn f)]
    (z {:a 1})))

(defn función-some-fn-4
  []
  (let [f (fn [x] (symbol? x))
        z (some-fn f)]
    (z \a)))

(defn función-some-fn-5
  []
  (let [f (fn [x] (int? x))
        g (fn [x] (number? x))
        z (some-fn f g)]
    (z 1)))

(defn función-some-fn-6
  []
  (let [f (fn [x] (int? x))
        g (fn [x] (char? x))
        z (some-fn f g)]
    (z 1.0)))

(defn función-some-fn-7
  []
  (let [f (fn [a] (int? a))
        g (fn [x] (string? x))
        z (some-fn f g)]
    (z 1M)))

(defn función-some-fn-8
  []
  (let [f (fn [x] (float? x))
        g (fn [x] (char? x))
        z (some-fn f g)]
    (z 1.0)))

(defn función-some-fn-9
  []
  (let [f (fn [x] (true? x))
        g (fn [x] (false? x))
        z (some-fn f g)]
    (z false)))

(defn función-some-fn-10
  []
  (let [f (fn [x] (seqable? x))
        g (fn [x] (sequential? x))
        z (some-fn f g)]
    (z [1 2 3])))

(defn función-some-fn-11
  []
  (let [f (fn [x] (list? x))
        g (fn [x] (vector? x))
        h (fn [x] (map? x))
        z (some-fn f g h)]
    (z '(1 2 3))))

(defn función-some-fn-12
  []
  (let [f (fn [x] (sequential? x))
        g (fn [x] (odd? x))
        h (fn [x] (list? x))
        z (some-fn f g h)]
    (z [1 2 3])))

(defn función-some-fn-13
  []
  (let [f (fn [a] (even? a))
        g (fn [x] (string? x))
        h (fn [x] (false? x))
        z (some-fn f g h)]
    (z 1)))

(defn función-some-fn-14
  []
  (let [f (fn [x] (true? x))
        z (some-fn f)]
    (z true)))

(defn función-some-fn-15
  []
  (let [f (fn [x] (sequential? x))
        z (some-fn f)]
    (z [5 4 3 1 2])))

(defn función-some-fn-16
  []
  (let [f (fn [x] (char? x))
        g (fn [x] (true? x))
        h (fn [x] (vector? x))
        i (fn [x] (odd? x))
        z (some-fn f)]
    (z \a)))

(defn función-some-fn-17
  []
  (let [f (fn [x] (char? x))
        g (fn [x] (string? x))
        h (fn [x] (false? x))
        i (fn [x] (number? x))
        z (some-fn f g h i)]
    (z "a")))

(defn función-some-fn-18
  []
  (let [f (fn [x] (list? x))
        g (fn [x] (vector? x))
        h (fn [x] (map? x))
        i (fn [x] (char? x))
        z (some-fn f g h i)]
    (z [ ])))

(defn función-some-fn-19
  []
  (let [f (fn [x] (int? x))
        z (some-fn f)]
    (z 1)))

(defn función-some-fn-20
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (vector? x))
        h (fn [x] (map? x))
        i (fn [x] (char? x))
        j (fn [x] (int? x))
        z (some-fn f g h i j)]
    (z #{"h" "o" "l" "a"})))

;Ejemplos:
(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-13)
(función-some-fn-12)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)